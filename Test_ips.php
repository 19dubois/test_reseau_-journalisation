<?php
 /**
   * Test_ips
   * 
   * 
   * @author     Marcellin DUBOIS <marcellin.dubois@orange.fr>
   */
class Test_ips
{
	private $Test_ips = [];
	 /**
       * 
       * Construct
       *
       */
	public function __construct ()
	{
			
	}
	 /**
       * 
       * lancerTest
       *
       * @param array $ips renvoie le tableau de la validité des ip.
       */
	public function lancerTest (array $ips)
	{
		for($i=0;$i<count($ips);$i++)
		{
			$output = "";
			$this->Test_ips[$i][0] = $ips[$i][0];
			if($ips[$i][1]!= "IP Erreur") 
			{
				$Resultat = exec("ping ".$ips[$i][0]." -c 1", $output);
				if(stristr($output[4], '100% packet loss') == TRUE || stristr($output[4], '+1 errors') == TRUE)
				{ 
					$this->Test_ips[$i][1] = "ERREUR";
				}else {
					$this->Test_ips[$i][1] = "OK";
				}	
			}else {
				$this->Test_ips[$i][1] = "ERREUR IP";			
			}
		}	
	}

	 /**
       * 
       * Retour
       *
       * @return array
       */
	public function Retour():array
	{
		return $this->Test_ips;	
	}
	
	
}


?> 
