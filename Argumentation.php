<?php
class Argumentation
{
	private $message = ["=> Syntaxe :php -f main.php  Arg1 Arg2\n    Arg1: 'Nom du fichier journal à générer'\n    Arg2: 'Nom du fichier contenant les adresses IP à tester'\n","<< ERREUR : LE FICHIER SOURCE 'liste.txt' N'EXISTE PAS\n","OK"];
	public function __construct ()
	{
		
	}
	
	public function verificationArgument ($arg)
	{
		if(($arg[1] == "") || ($arg[2] == "") || (count($arg) >= 4)) 
		{
			return $this->message[0];
		}elseif(!is_file($arg[1])) {
			return $this->message[1];
		}else {
			return $this->message[2];
		}
	}
	public function __destruct ()
	{
		
	}

}

?> 
