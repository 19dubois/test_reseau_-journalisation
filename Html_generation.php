<?php
class htmlGeneration
{
	private $html = "";
	private $fichier;

	public function __construct ($file)
	{
		$this->fichier = fopen($file, 'w+');
	}
	public function generateHtml ($table,$horaire)
	{
		$this->html = "<html lang='fr'>
		<head>
			<title>Vérification du fonctionnement des serveurs</title>
			<meta charset='UTF-8'>
		</head>
		<body>
			<div style='width:50%;background-image: url(https://dhh.com.hr/image/dhh-full.jpg);background-repeat: no-repeat;background-size: cover;'>
				<div style='margin-top:2%;padding:0.1%;'>
					<p style='font-size:30px;text-decoration:underline;text-align: center;color:red;'>SCRIPT PLANIFIE: Test du réseau</p>
				</div>
				<h3 style='margin-left:20%;color:white;'>Date: 
		" . $horaire . "</h3>
		<table style='margin: auto;width:80%;border:solid;border-width:1px;border-radius: 7px;background-color: #ffffffcc;'>
		<tr>
		<td style='border:solid;border-width:1px;'>
		<h1 style='font-size:25px;width: auto;margin-left:7%;text-decoration:underline;'>ADRESSES IP</h1>
		</td>
		<td style='border:solid;border-width:1px;'>
		<h1 style='font-size:25px;width: 150px;margin-left:7%;text-decoration:underline;'>STATUS</h1>
		</td>
		</tr>";
		for($i=0;$i<count($table);$i++)
		{
			$this->html = $this->html . "
			<tr>
			<td style='border:solid;border-width:1px;'>
			<h2 style='font-size:15px;width: 150px;margin-left:7%;'>" .$table[$i][0]. "
			</h2>
			</td>";
			if($table[$i][1] == "OK") 
			{
				$this->html = $this->html . "
				<td style='border:solid;border-width:1px;'>
				<h2 style='color:green;'>". $table[$i][1]."
				</h2>
				</td>
				</tr>";
			}else {
				$this->html = $this->html . "
				<td style='border:solid;border-width:1px;'>
				<h2 style='color:red;'>". $table[$i][1]."
				</h2>
				</td>
				</tr>";
			}
		}
		$this->html = $this->html . "
		</table>
		<div style='margin-top:2%;padding:0.1%;'>
		<h4 style='font-size:30px;text-decoration:underline;text-align: center;color:red;'>FIN DU TEST</h4>
		</div>
		</div>
		</body>
		</html>";
		fwrite($this->fichier, $this->html);
	}
	public function Retour () 
	{
		return $this->html;	
	}
	
	public function __destruct () 
	{
		fclose($this->fichier);	
	}
}

?> 
