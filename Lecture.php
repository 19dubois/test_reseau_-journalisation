<?php
class Lecture
{
	private $ips = [];
	private $Error = "";
	private $horaire = "";
	public function __construct ()
	{
		$date = date("d-m-Y");
		$heure = date("H:i");
		$this->horaire= $date . " à " . $heure;
	}
	public function Lire($f,$separateur)
	{
		$this->ips = file($f);
		for($i=0;$i < count($this->ips);$i++)
		{
	 		$this->ips[$i] = trim($this->ips[$i]);
		}
		for($i=0;$i < count($this->ips);$i++)
		{
	 		$this->ips[$i] = explode($separateur,$this->ips[$i]);
		}
	}
	public function verificationIps()
	{
		for($i=0;$i<count($this->ips);$i++) 
		{
			if (filter_var(implode($this->ips[$i]), FILTER_VALIDATE_IP))
        	{
				$this->ips[$i][1] = "IP VALIDE";        		
        	}else{
				$this->ips[$i][1] = "IP Erreur";
        	}
		}
		return $this->Error;
	}
	public function information() 
	{
		return $this->horaire;	
	}
	public function Retour() 
	{
		return $this->ips;	
	}
}

?> 
