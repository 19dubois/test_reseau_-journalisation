<?php
class Mail
{
	private $verificationMail = false;
	private $to = "";
	public function __construct ($mail)
	{
  		// Valider l'email
  		if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
    		$this->verificationMail = true;
    		$this->to = $mail;
  		}
  		
	}
	public function envoyer($message,$sujet) 
	{
		if($this->verificationMail == true) 
		{
			$sujet = utf8_decode($sujet);
			// L'en-tête Mail "Content-type" doit être défini :
			$headers  = "MIME-Version: 1.0 \r\n";
			$headers .= "Content-type: text/html; charset=UTF-8 \r\n";
			// En-têtes additionnels :
			$headers .= "To: ".$this->to."\r\n";     
			$headers .= "From: ".$this->to."\r\n";
			// Envoi du mail
			if (mail($this->to, $sujet, $message, $headers)) return "\n=> Courriel envoyé !!\n";     else  return "\n=> Erreur dans l'envoi du courriel !!\n";		
		}else {
			return "L'adresse mail n'est pas valide\n";
		}
	}
	public function __destruct() 
	{
		$this->verificationMail = false;
	}
}

?> 
